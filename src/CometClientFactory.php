<?php

namespace Drupal\farm_comet;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Factory class to get authenticated instance of the CometClient.
 */
class CometClientFactory {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructor for the CometClientFactory.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Returns a CometClient authenticated with the config email.
   *
   * @return \Drupal\farm_comet\CometClientInterface
   *   The CometClient.
   */
  public function getAuthenticatedClient() {
    $config = $this->configFactory->get('farm_comet.settings');
    $email = $config->get('email');
    return new CometClient($email);
  }

}
