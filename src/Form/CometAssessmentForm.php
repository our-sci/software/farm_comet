<?php

namespace Drupal\farm_comet\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Component\Serialization\Json;
use Drupal\farm_comet\CometAssessmentInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;

/**
 * CometAssessmentForm creates empirical and Daycent results.
 */
class CometAssessmentForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Comet Assessment service.
   *
   * @var \Drupal\farm_comet\CometAssessmentInterface
   */
  protected $cometAssessment;

  /**
   * Constructs a CometAssessmentForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\farm_comet\CometAssessmentInterface $comet_assessment
   *   The comet assessment service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, CometAssessmentInterface $comet_assessment) {
    $this->entityTypeManager = $entity_type_manager;
    $this->cometAssessment = $comet_assessment;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('farm_comet.assessment'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'farm_comet_assessment';
  }

  /**
   * Api View Data.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['asset'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Asset'),
      '#description' => $this->t('Asset Autocomplete'),
      '#target_type' => 'asset',
      '#selection_settings' => [
        'target_bundles' => ['plant'],
      ],
      '#required' => TRUE,
    ];

    $form['file_upload'] = [
      '#title' => $this->t('Upload Assessment file, requires a XML format'),
      '#type' => 'managed_file',
      '#upload_validators' => [
        'file_validate_extensions' => ['xml'],
      ],
      '#upload_location' => 'private://comet',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * Submit Form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Check if extension is a JSON format.
    if ($file_id = $form_state->getValue('file_upload')) {
      $asset_id = $form_state->getValue('asset');
      $asset = $this->entityTypeManager->getStorage('asset')->load($asset_id);

      $file = File::load($file_id[0]);//phpcs:ignore
      $file_data = file_get_contents($file->getFileUri());
      $xml = simplexml_load_string($file_data);
      $json = Json::encode($xml);
      $responseArray = Json::decode($json);

      if ($cropland = $responseArray["Cropland"]["ModelRun"]["Scenario"]) {

        $assessment_result = $this->cometAssessment->createAssessment($file_id[0], $asset, $cropland);

        if (is_null($assessment_result)) {
          $this->messenger()->addError("error");
        }

        // The Assessment Result should return an ID or an error.
        $this->messenger()->addMessage($this->t("Data has been saved."));

        // View page of the Assessment created.
        $view_page = "view.comet_assessment.page_1";

        // Redirect to the Comet Assessment view page.
        $form_state->setRedirect($view_page);
      }
      else {
        $this->messenger()->addError("Unavailable Cropland data");
      }
    }
  }

}
