<?php

namespace Drupal\farm_comet\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Comet settings form.
 */
class CometSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'farm_comet.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'farm_comet_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Email field.
    $config = $this->config(static::SETTINGS);

    $form['email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter email'),
      '#default_value' => $config->get('email'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $email = $form_state->getValue('email');

    $this->configFactory->getEditable(static::SETTINGS)
      ->set('email', $email)
      ->save();
    parent::submitForm($form, $form_state);
  }

}
