<?php

namespace Drupal\farm_comet\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\farm_comet\CometClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;

/**
 * CometApiViewForm creates empirical and Daycent results.
 */
class CometApiViewForm extends FormBase {

  /**
   * The Comet Client Service.
   *
   * @var \Drupal\farm_comet\CometClientInterface
   */
  protected $cometClient;

  /**
   * Constructs a cometClient object.
   *
   * @param \Drupal\farm_comet\CometClientInterface $comet_client
   *   The comet client service.
   */
  public function __construct(CometClientInterface $comet_client) {
    $this->cometClient = $comet_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('farm_comet.comet_client'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'farm_comet_apiview';
  }

  /**
   * Api View Data.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['file_upload'] = [
      '#title' => $this->t('Upload XML file format'),
      '#type' => 'managed_file',
      '#upload_validators' => [
        'file_validate_extensions' => ['xml'],
      ],
      '#upload_location' => 'private://comet',
    ];

    $form['last_cropland'] = [
      '#title' => $this->t('Enter last cropland run, a negative is required'),
      '#type' => 'number',
      '#default_value' => -1,
      '#max' => 0,
    ];

    $form['first_cropland'] = [
      '#title' => $this->t('Enter first cropland run, a negative is required.'),
      '#type' => 'number',
      '#default_value' => -1,
      '#max' => 0,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * Submit Form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($file_id = $form_state->getValue('file_upload')) {
      $file = File::load($file_id[0]);//phpcs:ignore
      $data = [
        'file_data' => file_get_contents($file->getFileUri()),
        'file_name' => $file->getFilename(),
        'last_cropland' => $form_state->getValue('last_cropland'),
        'first_cropland' => $form_state->getValue('first_cropland'),
      ];

      $comet_result = $this->cometClient->addToQueue($data);

      // The Comet API View result returns successful string or status code.
      if ($comet_result['status'] == TRUE) {
        $this->messenger()->addMessage($comet_result['message']);
      }
      else {
        $this->messenger()->addError($comet_result['message']);
      }
    }
  }

}
