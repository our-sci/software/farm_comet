<?php

namespace Drupal\farm_comet;

use GuzzleHttp\Client;

/**
 * Extends the Guzzle HTTP client with helper methods for the Comet API.
 *
 * See https://comet-farm.com/ApiMain.
 */
class CometClient extends Client implements CometClientInterface {

  /**
   * The comet email.
   *
   * @var string
   */
  protected $cometEmail;

  /**
   * The base URI of the Comet API.
   *
   * @var string
   */
  public static string $cometApiBaseUri = 'https://comet-farm.com/ApiMain/';

  /**
   * CometClient constructor.
   *
   * @param string $email
   *   Comet email.
   * @param array $config
   *   Guzzle client config.
   */
  public function __construct(string $email, array $config = []) {
    $this->cometEmail = $email;
    $default_config = [
      'base_uri' => self::$cometApiBaseUri,
      'http_errors' => FALSE,
    ];
    $config = $default_config + $config;
    parent::__construct($config);
  }

  /**
   * Returns Comet add to queue results.
   */
  public function addToQueue(array $queueData) {
    $defaultQueueData = [
      'file_keys' => '["file0"]',
      'url' => '',
      'email' => $this->cometEmail,
      'last_daycent_input' => 0,
      'first_daycent_input' => 0,
    ];

    $queueData = $queueData + $defaultQueueData;

    $res = $this->request('POST', 'AddToQueue', [
      'multipart' => [
        [
          'name' => 'file0',
          'contents' => $queueData['file_data'],
          'filename' => $queueData['file_name'],
        ],
        [
          'name' => 'FileKeys',
          'contents' => $queueData['file_keys'],
        ],
        [
          'name' => 'LastCropland',
          'contents' => $queueData['last_cropland'],
        ],
        [
          'name' => 'FirstCropland',
          'contents' => $queueData['first_cropland'],
        ],
        [
          'name' => 'email',
          'contents' => $queueData['email'],
        ],
        [
          'name' => 'url',
          'contents' => '',
        ],
        [
          'name' => 'LastDaycentInput',
          'contents' => $queueData['last_daycent_input'],
        ],
        [
          'name' => 'FirstDaycentInput',
          'contents' => $queueData['first_daycent_input'],
        ],
      ],
    ]);

    if ($res->getStatusCode() === 200) {
      $resString = (string) $res->getBody();

      // Verify that the response result is valid.
      $resultStatus = $this->validateResult($resString);

      return ['status' => $resultStatus, 'message' => $resString];
      ;
    }
    return ['status' => FALSE, 'message' => 'unsuccessful'];
  }

  /**
   * Returns a boolean after verifying result.
   */
  private function validateResult(String $str) {
    // Verify that string begins with https://comet-farm.com.
    $strStartWith = '/^Files have been successfully added to the queue/i';
    $strMatch = preg_match($strStartWith, $str);
    // Verify that the string is unsuccessful.
    if ($strMatch != 1) {
      return FALSE;
      ;
    }

    // Add all the values of the process time together.
    $processTimerValues = preg_replace('/[^0-9]/', '', $str);

    // Time values should exceed 0 if successful.
    $statusResult = $processTimerValues > 000000000000 ? TRUE : FALSE;

    return $statusResult;
  }

}
