<?php

namespace Drupal\farm_comet;

use GuzzleHttp\ClientInterface;

/**
 * Interface for the Comet client.
 */
interface CometClientInterface extends ClientInterface {

  /**
   * Returns the Comet data.
   *
   * @param array $queue_data
   *   The queue data.
   */
  public function addToQueue(array $queue_data);

}
