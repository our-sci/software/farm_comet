<?php

namespace Drupal\farm_comet;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Database\Connection;
use Drupal\asset\Entity\AssetInterface;

/**
 * A service for interacting with Comet Assessment.
 */
class CometAssessment implements CometAssessmentInterface {

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs the CFTAssessment class.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(Connection $connection, TimeInterface $time) {
    $this->database = $connection;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function createAssessment($file_id, AssetInterface $asset, $data = []) {

    if (!$data) {
      return NULL;
    }

    $query = $this->database;

    $assessment_id = NULL;

    foreach ($data as $item) {
      // Based on the condition, generate an Assessment ID once.
      switch ($item) {
        case $item['Carbon'] && !$assessment_id:
        case $item['CO2'] && !$assessment_id:
        case $item['N2O'] && !$assessment_id:
        case $item['CH4'] && !$assessment_id:
          $assessment_id = $this->getAssessmentId($file_id, $asset->id());
          break;
      };

      if ($carbon = $item['Carbon']) {
        $query_carbon = $query->insert('farm_comet_assessment_carbon');
        $query_carbon->fields([
          'assessment_id' => $assessment_id,
          'scenario' => $item['@attributes']['name'],
          'soil_carbon' => $carbon['SoilCarbon'],
          'biomass_burning_carbon' => $carbon['BiomassBurningCarbon'],
          'soil_carbon_stock_2000' => $carbon['SoilCarbonStock2000'],
          'soil_carbon_stock_begin' => $carbon['SoilCarbonStockBegin'],
          'soil_carbon_stock_end' => $carbon['SoilCarbonStockEnd'],
        ]);
        $query_carbon->execute();
      }

      if ($co2 = $item['CO2']) {
        $query_co2 = $query->insert('farm_comet_assessment_co2');
        $query_co2->fields([
          'assessment_id' => $assessment_id,
          'scenario' => $item['@attributes']['name'],
          'liming_co2' => $co2['LimingCO2'],
          'urea_fertilization_co2' => $co2['UreaFertilizationCO2'],
          'drained_organic_soils_co2' => $co2['DrainedOrganicSoilsCO2'],
        ]);
        $query_co2->execute();
      }

      if ($n2o = $item['N2O']) {
        $query_n2o = $query->insert('farm_comet_assessment_n2o');
        $query_n2o->fields([
          'assessment_id' => $assessment_id,
          'scenario' => $item['@attributes']['name'],
          'soil_n2o' => $n2o['SoilN2O'],
          'soil_n2o_direct' => $n2o['SoilN2O_Direct'],
          'soil_n2o_indirect_volatilization' => $n2o['SoilN2O_Indirect_Volatilization'],
          'soil_n2o_indirect_leaching' => $n2o['SoilN2O_Indirect_Leaching'],
          'wetland_rice_cultivation_n2o' => $n2o['WetlandRiceCultivationN2O'],
          'biomass_burning_n2o' => $n2o['BiomassBurningN2O'],
          'drained_organic_soils_n2o' => $n2o['DrainedOrganicSoilsN2O'],
        ]);
        $query_n2o->execute();
      }

      if ($ch4 = $item['CH4']) {
        $query_ch4 = $query->insert('farm_comet_assessment_ch4');
        $query_ch4->fields([
          'assessment_id' => $assessment_id,
          'scenario' => $item['@attributes']['name'],
          'soil_ch4' => $ch4['SoilCH4'],
          'wetland_rice_cultivation_ch4' => $ch4['WetlandRiceCultivationCH4'],
          'biomass_burning_ch4' => $ch4['BiomassBurningCH4'],
        ]);
        $query_ch4->execute();
      }
    }
    return $assessment_id;
  }

  /**
   * Returns Assessment ID.
   */
  protected function getAssessmentId($file_id, string $id) {
    // Insert Assessment on every successful submit.
    $query_assessment = $this->database->insert('farm_comet_assessment');
    $query_assessment->fields(['asset_id', 'file_entity', 'timestamp']);
    $query_assessment->values([$id, $file_id, $this->time->getCurrentTime()]);

    $assessment_id = $query_assessment->execute();

    return $assessment_id;
  }

}
