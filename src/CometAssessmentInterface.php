<?php

namespace Drupal\farm_comet;

use Drupal\asset\Entity\AssetInterface;

/**
 * An interface for the CometAssessment service.
 */
interface CometAssessmentInterface {

  /**
   * Create an comet assessment.
   *
   * @param int $file_id
   *   Comet Result File ID.
   * @param \Drupal\asset\Entity\AssetInterface $asset
   *   The asset to create the assessment.
   * @param array $data
   *   Assessment Data.
   *
   * @return int|string
   *   The base assessment ID or FALSE if the assessment could not be created.
   */
  public function createAssessment($file_id, AssetInterface $asset, array $data = []);

}
