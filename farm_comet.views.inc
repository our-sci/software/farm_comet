<?php

/**
 * @file
 * Provide Views data for farm_comet.module.
 */

/**
 * Implements hook_views_data().
 */
function farm_comet_views_data() {

  // Build array of views data.
  $data = [];

  // Base Comet assessment table.
  $data['farm_comet_assessment'] = [
    'table' => [
      'group' => t('Comet Assessments'),
      'base' => [
        'title' => t('Comet Assessments'),
        'help' => t('Base table for all Comet assessments.'),
      ],
    ],
    'assessment_id' => [
      'title' => t('Assessment ID'),
      'help' => t('Unique ID of the base assessment.'),
      'field' => [
        'id' => 'numeric',
        'click sortable' => TRUE,
      ],
      'argument' => [
        'id' => 'standard',
      ],
    ],
    'asset_id' => [
      'title' => t('Asset ID'),
      'help' => t('ID of the location asset associated with the assessment.'),
      'relationship' => [
        'base' => 'asset_field_data',
        'base_field' => 'asset_id',
        'id' => 'standard',
        'label' => t('Location asset'),
      ],
    ],
    'timestamp' => [
      'title' => t('Assessment timestamp'),
      'help' => t('Timestamp the Comet assessment was taken.'),
      'field' => [
        'id' => 'date',
        'click sortable' => TRUE,
      ],
    ],
  ];

  $data['farm_comet_assessment_carbon'] = [
    'table' => [
      'group' => t('Comet Assessments'),
    ],
    'assessment_id' => [
      'title' => t('Assessment ID'),
      'help' => t('ID of the base Comet Assessment.'),
      'relationship' => [
        'base' => 'farm_comet_assessment',
        'base_field' => 'assessment_id',
        'id' => 'standard',
        'label' => t('Base Assessment'),
      ],
    ],
    'scenario' => [
      'title' => t('Scenario'),
      'help' => t('Data Scenario'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'soil_carbon' => [
      'title' => t('Soil Carbon'),
      'help' => t('Soil Carbon'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'biomass_burning_carbon' => [
      'title' => t('Biomass Burning Carbon'),
      'help' => t('Biomass Burning Carbon'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'soil_carbon_stock_2000' => [
      'title' => t('Soil Carbon Stock 2000'),
      'help' => t('Soil Carbon Stock 2000'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'soil_carbon_stock_begin' => [
      'title' => t('Soil Carbon Stock Begin'),
      'help' => t('Soil Carbon Stock Begin'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'soil_carbon_stock_end' => [
      'title' => t('Soil Carbon Stock End'),
      'help' => t('Soil Carbon Stock End'),
      'field' => [
        'id' => 'standard',
      ],
    ],
  ];

  $data['farm_comet_assessment']['farm_comet_assessment_carbon'] = [
    'title' => t('Assessment Carbon Data'),
    'help' => t('COMET Assessment Carbon'),
    'relationship' => [
      'label' => t('Assessment Carbon'),
      'id' => 'standard',
      'base' => 'farm_comet_assessment_carbon',
      'base field' => 'assessment_id',
      'field' => 'assessment_id',
    ],
  ];

  $data['farm_comet_assessment_co2'] = [
    'table' => [
      'group' => t('Comet Assessments'),
    ],
    'assessment_id' => [
      'title' => t('Assessment ID'),
      'help' => t('ID of the base Comet Assessment.'),
      'relationship' => [
        'base' => 'farm_comet_assessment',
        'base_field' => 'assessment_id',
        'id' => 'standard',
        'label' => t('Base Assessment'),
      ],
    ],
    'scenario' => [
      'title' => t('Scenario'),
      'help' => t('Data Scenario'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'liming_co2' => [
      'title' => t('Liming CO2'),
      'help' => t('Liming CO2'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'urea_fertilization_co2' => [
      'title' => t('Urea Fertilization CO2'),
      'help' => t('Urea Fertilization CO2'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'drained_organic_soils_co2' => [
      'title' => t('Drained Organic Soils CO2'),
      'help' => t('Drained Organic Soils CO2'),
      'field' => [
        'id' => 'standard',
      ],
    ],
  ];

  $data['farm_comet_assessment']['farm_comet_assessment_co2'] = [
    'title' => t('Assessment CO2 Data'),
    'help' => t('COMET Assessment CO2'),
    'relationship' => [
      'label' => t('Assessment CO2'),
      'id' => 'standard',
      'base' => 'farm_comet_assessment_co2',
      'base field' => 'assessment_id',
      'field' => 'assessment_id',
    ],
  ];

  $data['farm_comet_assessment_n2o'] = [
    'table' => [
      'group' => t('Comet Assessments'),
    ],
    'assessment_id' => [
      'title' => t('Assessment ID'),
      'help' => t('ID of the base Comet Assessment.'),
      'relationship' => [
        'base' => 'farm_comet_assessment',
        'base_field' => 'assessment_id',
        'id' => 'standard',
        'label' => t('Base Assessment'),
      ],
    ],
    'scenario' => [
      'title' => t('Scenario'),
      'help' => t('Data Scenario'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'soil_n2o' => [
      'title' => t('Soil N2O'),
      'help' => t('Soil N2O'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'soil_n2o_direct' => [
      'title' => t('Soil N2O Direct'),
      'help' => t('Soil N2O Direct'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'soil_n2o_indirect_volatilization' => [
      'title' => t('Soil N2O Indirect Volatilization'),
      'help' => t('Soil N2O Indirect Volatilization'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'soil_n2o_indirect_leaching' => [
      'title' => t('Soil N2O Indirect Leaching'),
      'help' => t('Soil N2O Indirect Leaching'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'wetland_rice_cultivation_n2o' => [
      'title' => t('Wetland Rice Cultivation N2O'),
      'help' => t('Wetland Rice Cultivation N2O'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'biomass_burning_n2o' => [
      'title' => t('Biomass Burning N2O'),
      'help' => t('Biomass Burning N2O'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'drained_organic_soils_n2o' => [
      'title' => t('Drained Organic Soils N2O'),
      'help' => t('Drained Organic Soils N2O'),
      'field' => [
        'id' => 'standard',
      ],
    ],
  ];

  $data['farm_comet_assessment']['farm_comet_assessment_n2o'] = [
    'title' => t('Assessment N2O Data'),
    'help' => t('COMET Assessment N2O'),
    'relationship' => [
      'label' => t('Assessment N2O'),
      'id' => 'standard',
      'base' => 'farm_comet_assessment_n2o',
      'base field' => 'assessment_id',
      'field' => 'assessment_id',
    ],
  ];

  $data['farm_comet_assessment_ch4'] = [
    'table' => [
      'group' => t('Comet Assessments'),
    ],
    'assessment_id' => [
      'title' => t('Assessment ID'),
      'help' => t('ID of the base Comet Assessment.'),
      'relationship' => [
        'base' => 'farm_comet_assessment',
        'base_field' => 'assessment_id',
        'id' => 'standard',
        'label' => t('Base Assessment'),
      ],
    ],
    'scenario' => [
      'title' => t('Scenario'),
      'help' => t('Data Scenario'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'soil_ch4' => [
      'title' => t('Soil CH4'),
      'help' => t('Soil CH4'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'wetland_rice_cultivation_ch4' => [
      'title' => t('Wetland Rice Cultivation CH4'),
      'help' => t('Wetland Rice Cultivation CH4'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'biomass_burning_ch4' => [
      'title' => t('Biomass Burning CH4'),
      'help' => t('Biomass Burning CH4'),
      'field' => [
        'id' => 'standard',
      ],
    ],
  ];

  $data['farm_comet_assessment']['farm_comet_assessment_ch4'] = [
    'title' => t('Assessment CH4 Data'),
    'help' => t('COMET Assessment CH4'),
    'relationship' => [
      'label' => t('Assessment CH4'),
      'id' => 'standard',
      'base' => 'farm_comet_assessment_ch4',
      'base field' => 'assessment_id',
      'field' => 'assessment_id',
    ],
  ];
  return $data;
}
